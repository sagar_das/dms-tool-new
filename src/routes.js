import HomeComponent from "./components/Home.vue";
import ValidationComponent from "./components/Validation.vue";
import LoginComponent from "./components/Login.vue";
import GFComponent from "./components/GF.vue";
import UploadComponent from "./components/Upload.vue";

const routes = [
  // {
  //   path: "/",
  //   redirect: {
  //     name: "login"
  //   }
  // },
  {
    path: "/login",
    name: "login",
    component: LoginComponent
  },
  {
    path: "/home",
    name: "home",
    component: HomeComponent,
    props: true,
    children: [
      {
        path: "gf",
        name: "gf",
        component: GFComponent
      },
      {
        path: "validation",
        name: "validation",
        component: ValidationComponent
      },
      {
        path: "upload",
        name: "upload",
        component: UploadComponent
      }
    ]
  }
];

export default routes;
