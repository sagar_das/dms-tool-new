import Vue from "vue";
import axios from "axios";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "@mdi/font/css/materialdesignicons.css";
import VueRouter from "vue-router";
import AppComponent from "./App.vue";
import Routes from "./routes";
import { store } from "./store/index";
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
Vue.use(VueSidebarMenu)

Vue.use(Vuetify, {
  iconfont: "mdi"
});
Vue.use(VueRouter);
Vue.prototype.$http = axios;

if (window.location.href.indexOf('localhost') > -1)
  Vue.prototype.$baseUrl = 'http://localhost:1337'; //Dev
else
  Vue.prototype.$baseUrl = 'https://developer.commercecx.com/'; //Prod 'developer' should be changed to dms-server in future 

Vue.config.productionTip = false;

const router = new VueRouter({
  mode: 'history',
  routes: Routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.isLoggedIn) {
      next({
        name: 'login'
      })
    } else {
      next()
    }
  }
  if (to.matched.some(record => record.meta.requiresAdmin)) {
    // this route requires auth, check if logged in
    // if not, redirect to home page.
    if (!store.getters.loggedUser.type == 'admin') {
      next({
        name: 'home'
      })
    } else {
      next()
    }
  }
  else {
    next() // make sure to always call next()!
  }
})

export const serverBus = new Vue();

new Vue({
  router,
  store,
  render: h => h(AppComponent)
}).$mount("#app");
