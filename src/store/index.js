import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    isDark: false,
    user: {
      isAuthenticated: false,
      isDemo: false,
      token: "",
      org: "",
      userName: "",
      id: "",
      version: ""
    },
    loader: {
      isVisible: false,
      text: "",
      isStep: false,
      buffer: 100,
      bufferValue: 100
    },
    toast: {
      isVisible: false,
      isCloseVisible: false,
      timeout: 3000,
      text: "",
      error: ""
    },
    confirmation: {
      isVisible: false,
      text: "",
      heading: ""
    },
    exportData: {},
    objectsData: {
      objectList: [],
      headers: [
        {
          text: "Object Name",
          value: "label",
          width: "25%"
        },
        {
          text: "Object Api Name",
          value: "apiName",
          width: "30%"
        },
        {
          text: "Object Category",
          value: "objectType",
          width: "20%"
        },
        {
          text: "Sequence",
          value: "seq",
          width: "5%"
        },
        {
          text: "Fields",
          value: "fieldList",
          width: "15%"
        }
      ],
      selected: [],
      search: "",
      pagination: { "rowsPerPage": 5, "descending": false, "page": 1, "sortBy": "", "totalItems": 0 },
      objectName: ""
    },
    fieldKeys: {
      label: "Field Name",
      apiName: "API Name",
      sdataType: "Data Type",
      description: "Description",
      IsRequired: "Is Required",
      picklistValue: "Picklist Values"
    },
    templateKeys: {
      Id: "id",
      Name: "templateName",
      Type__c: "templateType",
      Selected_Objects__c: "objectList",
      LastModifiedDate: "lastModifiedDate",
      CreatedBy: "createdBy"
    }
  },
  mutations: {
    setLoader(state, payload) {
      if (payload.isStep !== undefined) {
        state.loader.isStep = payload.isStep;
        if (payload.isStep == true) {
          state.loader.buffer = ((payload.buffer * 100) / payload.bufferValue);
          state.loader.bufferValue = 100;
        }
      }
      if (payload.isVisible !== undefined)
        state.loader.isVisible = payload.isVisible;
      if (payload.text !== undefined)
        state.loader.text = payload.text;
    },
    setUser(state, payload) {
      state.user = payload;
    },
    setToast(state, payload) {
      state.toast = payload;
    },
    setDark(state, payload) {
      state.isDark = payload;
    },
    setConfirmation(state, payload) {
      state.confirmation = payload;
    },
    setObjectsData(state) {
      state.objectsData.pagination = { "rowsPerPage": 5, "descending": false, "page": 1, "sortBy": "", "totalItems": 0 };
      state.objectsData.search = "";
    }
  },
  getters: {
    loader: state => state.loader,
    user: state => state.user,
    toast: state => state.toast,
    isDark: state => state.isDark,
    objectsData: state => state.objectsData,
    confirmation: state => state.confirmation
  }
});
